import requests
import json


# url = 'https://gitlab.com/api/v4/groups/60147283/projects?pagination=keyset&per_page=1&order_by=id&sort=asc'

# функция принимает на вход урл, выдает список имен всех проектов в группе
def get_projects_names_gitlab(url):
    while True:
        response = requests.get(url)
        result = json.loads(response.text)
        head = response.headers
        print(result[0]['name'])
        # проверка наличия ссылки на следующую страницу в заголовке
        if 'rel="next"' not in head['Link']:
            break
        # перебор ссылок в заголовке, если есть ссылка на следующую страницу, то меняем урл на нее
        for j in head['Link'].split(','):
            if 'rel="next"' in j:
                url = j.split(';')[0].replace(' ', '')[1:-1]


get_projects_names_gitlab(
    'https://gitlab.com/api/v4/groups/60147283/projects?pagination=keyset&per_page=1&order_by=id&sort=asc')
